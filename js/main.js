// main.js

( () => {
	
	var slideIndex = 1;
	// set up the footer logos to change their color on mouseover
	var initFooterLogos = function(){
		let footerLogos = document.getElementsByClassName("footerLogo");
		// console.log(footerLogos)
		for( let i = 0; i < footerLogos.length; i++ ){
			let logoImage = footerLogos[i].getAttribute("src");
			footerLogos[i].addEventListener("mouseenter", (e) => {
				e.target.setAttribute("src", logoImage.split(".svg")[0] + "_purple.svg");
			});
			footerLogos[i].addEventListener("mouseleave", (e) => {
				e.target.setAttribute("src", logoImage);
			});
		}
	};

	// refresh the team graph for the project phase provided by the time slider
	var refreshNodes = function(activePhase){
		activePhase = Number(activePhase);
		for(let i = 0; i < allNodes.length; i++){
			if( allNodes[i].phase.indexOf(activePhase) != -1 && !allNodes[i].active ){
				// add nodes to Dataset
				data.nodes.add( [ allNodes[i] ] );
				allNodes[i].active = true;
			}
			else if(allNodes[i].phase.indexOf(activePhase) == -1 && allNodes[i].active){
				// remove nodes from Dataset
				data.nodes.remove( allNodes[i].id );
				allNodes[i].active = false;
			}
		}
	}

	// refresh the project phase description, is fired when time slider changes
	var refreshPhaseDescription = function(activePhase){
		if( document.getElementById("phaseDescriptionWrapper").querySelector(".phaseDescription.active") ){
			document.getElementById("phaseDescriptionWrapper").querySelector(".phaseDescription.active").classList.remove("active");
			document.getElementById("phaseDescription" + activePhase).classList.add("active");
		}
	}

	// project description slides
	var slideInterval = setInterval( () => {
		showSlides(slideIndex += 1);
	}, 20000);

	function plusSlides(n) {
	  showSlides(slideIndex += n);
	}

	function currentSlide(n) {
	  showSlides(slideIndex = n);
	}

	function showSlides(n) {
	  var slides = document.getElementsByClassName("textSlides");
	  var dots = document.querySelectorAll(".slideDots p");
	  dots.forEach( (dot) => {
	  	dot.addEventListener("click", () => {
	  		clearInterval(slideIndex);
	  		slideIndex = dot.getAttribute("index");
	  		showSlides( slideIndex );
	  		clearInterval(slideIndex);
	  	})
	  } );
	  if (n > slides.length) { slideIndex = 1 } // on end return to start
	  if (n < 1) { slideIndex = slides.length } // left from start go to end
	  document.querySelector(".textSlides.active").style.opacity = 0.01; 
	  document.querySelector(".slideDots p.active").classList.remove("active"); 
	  dots[slideIndex-1].classList.add("active");
	  setTimeout( () => {
		  for (let i = 0; i < slides.length; i++) {
		      slides[i].classList.remove("active");
		  }
		  slides[slideIndex-1].classList.add("active");
		  setTimeout( () => {
		  	slides[slideIndex-1].style.opacity = 1;
		  }, 10)
	  }, 400 )
	}

	// vis network options
	var options = {
	  width: '100%',
	  height: '100%',
	  nodes:{
	    shape: 'dot',
	    size: 6,
	    borderWidth: 0,
	    chosen:{
	    	node: function(values, id, selected, hovering){
	    		// console.log( values );
	    		if( (hovering || selected) ){
	    			let thisnode = data.nodes.get(id);
					if(thisnode.selectable == true){
						// console.log("selectable");
						values.borderWidth = 5;
						if(document.querySelector(".teampartners .profile.active")){
							document.querySelector(".teampartners .profile.active").classList.remove("active");								
						}
						document.getElementById("profile" + thisnode.label ).classList.add("active");
					}
	    		} // if
    		} // node
	    }, // chosen
	    color: {
	    	border: '#7b8fff',
	    	highlight: {
	    		border: '#7b8fff',
	    		background: 'transparent',
	    	}
	    },
	    font:{
	    	color: '#999',
	    	size: 0,
	    	align: 'left',
	    	face: 'sans-serif',
	    	vadjust: 2
	    },
	    brokenImage: './img/team/none.svg',
	    margin: 10,
	  },
	  edges:{
	    width:1,
	    color: 'black',
	    length: 10,
	    smooth: {
	      enabled: false
	    },
	    selectionWidth: 0,
	    hoverWidth: 0,
	  },
	  interaction:{
	  	dragView: false,
	  	zoomView: false,
	  	hover: true,
	  	navigationButtons: true
	  },
	  physics: {
	  	solver: 'barnesHut',
	    maxVelocity: 2,
	    barnesHut: {
	    	springLength: 100,
	    	gravitationalConstant: -15000,
	    	springConstant: 0.04,
	    	avoidOverlap: 1
	    }
	  },
	  layout:{
	    hierarchical: {
	    	enabled: false,
	    	direction: 'LR',
	    	// clusterThreshold: 3
	    }
	  }
	};

	// node data for the team graph
	var allNodes = [
        { id: 1, selectable: false, label: "", phase: [1,2,3,4], size: 60, fixed: true, color:'#7b8fff', font: { color:'#7b8fff'}, shape: 'image', image: './img/ars_circle.svg', x: 0, y: 0 },
        { id: 2, selectable: false, title: "The core research team", label: "Core Team", phase: [1,2,3,4], size: 20, shape: 'box', color: '#7b8fff', x: 0, y: -250, fixed: true, font: {color: 'white', size: 14 } },
        { id: 3, selectable: false, title: "External academic partners",label: "Academic partners", phase: [2,3,4], shape: 'box', borderWidth: 1, color: { background: 'white', border: '#7b8fff'}, x: -300, y: 250, fixed: true, font: {color: '#7b8fff', size: 14} },
        { id: 33, selectable: false, title: "External non-academic partners", label: "Non-Academic partners", phase: [2,3,4], shape: 'box', borderWidth: 1, color: { background: 'white', border: '#7b8fff'}, x: 300, y: 250, fixed: true, font: {color: '#7b8fff', size: 14} },
        
        { id: 333, selectable: false, title:"External academic partners", label: "Academic partners", phase: [1], shape: 'box', shapeProperties: { borderDashes: [5,5] }, borderWidth: 1, color: { background: 'white', border: '#7b8fff'}, x: -300, y: 250, fixed: true, font: {color: '#7b8fff', size: 14} },
        { id: 3333, selectable: false, title: "External non-academic partners", label: "Non-Academic partners", phase: [1], shape: 'box', shapeProperties: { borderDashes: [5,5] }, borderWidth: 1, color: { background: 'white', border: '#7b8fff'}, x: 300, y: 250, fixed: true, font: {color: '#7b8fff', size: 14} },

        { id: 4, selectable: false, title:"Technische Universität Berlin",label: "TU Berlin", phase: [1,2,3,4], size: 25, shape: 'image', image: './img/logo_tu_purple_graph.svg', fixed: true, y:-250, x:-250 },
        { id: 41, selectable: true, title:"Prof. Dr.-Ing. Jörg Gleiter", label: "JG", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/jg.jpg', color: {background: 'transparent'}, y:-200, x:-150},
        { id: 42, selectable: true, title: "Dr.-Ing. Michael Dürfeld",label: "MD", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/md.jpg', color: {background: 'transparent'}, y:-250, x:-420, fixed:true },
        { id: 422, selectable: false, label: "", phase: [1,2,3,4], size: 30, shape: 'dot', size: 0, color: 'transparent', y:-250, x:-320, fixed:true},
        { id: 43, selectable: true, title:"M.Sc. Zead Rahman",label: "ZR", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/zr.jpg', color: {background: 'transparent'}, y:-300, x:-250},
        
        // { id: 98, label: "Free Collaborators", phase: [1,2,3,4], size: 15, shape: 'dot', shapeProperties: { borderDashes: [5,5] }, y: -200, color: { background: 'white', border: '#999'}, borderWidth: 1, },
        { id: 99, selectable: true, title: "Dr. Christian Stein", label: "CS", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/cs_alt.jpg', color: {background: 'transparent'}, y: -350, x:-300},

        { id: 5, selectable: false, title: "Universität der Künste Berlin", label: "UdK Berlin", phase: [1,2,3,4], size: 20, shape: 'image', image: './img/logo_udk_purple.svg', color: {background: 'transparent'}, fixed: true, y:-250, x:250},
        { id: 51, selectable: true, title:"Prof. Dr. Norbert Palz",label: "NP", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/np.jpg', color: {background: 'transparent'}, shapeProperties:{interpolation: false}, y:-200, x:250},
        { id: 52, selectable: true, title: "Prof. Dr. Susanne Hauser",label: "SH", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/sh.jpg', color: {background: 'transparent'}, shapeProperties:{interpolation: false}, y:-200, x:250},
        { id: 53, selectable: true, title: "M.A. Ferdinand List",label: "FL", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/fl.jpg', color: {background: 'transparent'}, y:-200, x:250},
        { id: 54, selectable: true, title: "B.A. Niklas Thran",label: "NT", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/nt.jpg', color: {background: 'transparent'}, y:-200, x:250},
        { id: 55, selectable: true, title: "Renata Altenfelder Dias",label: "RD", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/rd_crop.jpg', color: {background: 'transparent'}, y:-200, x:250},
        // { id: 56, selectable: true, label: "PF", phase: [1,2,3,4], size: 30, shape: 'circularImage', image: './img/team/none.svg', color: {background: 'transparent'}, y:-200, x:250},

        { id: 6, selectable: true, title:"Fachhochschule Potsdam", label: "MP", size: 30, phase: [2,3,4],  shape: 'image', image: './img/partners/fhp.svg', color: {background: 'transparent'}, x: -300, y: 100 },
        // { id: 7, label: "Prof. Kai Kummert\nArchitecture and Building Technology\nBeuth University of Applied Sciences", size: 20, phase: [3,4],  shape: 'image', image: './img/partners/beuth.svg', color: 'transparent', x: -300, y: 200},
        { id: 77, selectable: true, title:"Berliner Hochschule für Technik",label: "SJ", size: 30, phase: [2,3,4],  shape: 'image', image: './img/partners/bht_small.svg', color: {background: 'transparent'}, x: -300, y: 200},
        { id: 8,  selectable: true, title: "BTU Cottbus-Senftenberg", label: "IV", size: 17, phase: [2,3,4],  shape: 'image', image: './img/partners/btu.png', color: {background: 'transparent'}, shapeProperties:{interpolation: false}, x: -300, y: 200},
        { id: 9,  selectable: true, title: "Design & Computation UdK Berlin & TU Berlin", label: "DC", size: 25, phase: [3,4],  shape: 'image', image: './img/partners/designcomputation.svg', color: {background: 'transparent'}, x: -300, y: 200},
        

        { id: 10, selectable: true, title: "Deutsches Architekturzentrum", label: "DAZ", size: 25, phase: [4],  shape: 'image', image: './img/partners/DAZ.svg', color: 'transparent', x: 300, y: 200},
        { id: 11, selectable: true, title: "Netzwerk Architekturwissenschaft e.V.", label: "NAW", size: 30, phase: [2,3,4],  shape: 'image', image: './img/partners/netzwerk_architekturwissenschaft.svg', color: 'transparent', x: 300, y: 200},
        { id: 12, selectable: true, title: "Hybrid Plattform", label: "HP", size: 30, phase: [4],  shape: 'image', image: './img/partners/hybrid_logo.png', color: 'transparent', x: -300, y: 200},
        { id: 13, selectable: true, title: "Wolkenkuckucksheim", label: "WKH", size: 30, phase: [3,4],  shape: 'image', image: './img/partners/wolkenkuckucksheim.jpg', color: 'transparent', x: 300, y: 200},
        { id: 14, selectable: true, title: "Fathomable VR Development", label: "FA", size: 20, phase: [3,4],  shape: 'image', image: './img/partners/fathomable.png', color: 'transparent', shapeProperties:{interpolation: false}, x: 300, y: 200},


        { id: 999, selectable: false, title: "Open partner slot",label: "...", margin: 20, phase: [2,3,4],  shape: 'circle', shapeProperties: { borderDashes: [5,5] }, x: 300, y: 200, borderWidth: 1, color: { background: 'white', border: '#7b8fff'} },
        { id: 998, selectable: false, title: "Open partner slot", label: "...", margin: 20, phase: [2,3,4],  shape: 'circle', shapeProperties: { borderDashes: [5,5] }, x: 400, y: 400, borderWidth: 1, color: { background: 'white', border: '#7b8fff'} },
        { id: 997, selectable: false, title: "Open partner slot", label: "...", margin: 20, phase: [2,3,4],  shape: 'circle', shapeProperties: { borderDashes: [5,5] }, x: -400, y: 400, borderWidth: 1, color: { background: 'white', border: '#7b8fff'} }
    ];

	// composed graph data with nodes and edges
	var data = {
		nodes: new vis.DataSet(),
		edges: new vis.DataSet([
	        { from: 2, to: 1, endPointOffset: { from: 50, to: 50 }, arrowStrikethrough: true, color: '#999' }, // core team to ARS
	        { from: 3, to: 1, color: '#999' }, // academic partners to ARS
	        { from: 33, to: 1, color: '#999' }, // non-academic partners to ARS

	        { from: 333, to: 1, color: '#7b8fff', dashes: [5,5], width: 1 }, // non-academic partners to ARS
	        { from: 3333, to: 1, color: '#7b8fff', dashes: [5,5], width: 1 }, // non-academic partners to ARS

	        { from: 4, to: 2, color: '#999'}, // tu berlin to core team
	        { from: 41, to: 4, color: '#999'}, // jg to tu
	        { from: 42, to: 422, color: '#999'}, // md to zwischenknoten
	        { from: 422, to: 4, color: '#999', length: 0.001}, // zwischenknoten to tu
	        { from: 43, to: 4, color: '#999'}, // zr to tu

	        // { from: 98, to: 2, color: '#999', dashes: [5,5]}, // free collab to core team
	        { from: 99, to: 422, color: '#999', dashes: [5,5]}, // cs to free collab
	        
	        { from: 5, to: 2, color: '#999' }, // udk to core team
	        { from: 5, to: 51, color: '#999' },
	        { from: 5, to: 52, color: '#999' },
	        { from: 5, to: 53, color: '#999' },
	        { from: 5, to: 54, color: '#999' },
	        { from: 5, to: 55, color: '#999' },
	        // { from: 5, to: 56, color: '#999' },

	        { from: 6, to: 3, color: '#999' },
	        { from: 7, to: 3, color: '#999' }, // Kummert to academic partners
	        { from: 77, to: 3, color: '#999' }, // Junker to Academic Partners
	        { from: 9, to: 3, color: '#999' }, // Computational design
	        { from: 8, to: 3, color: '#999' },
	        { from: 12, to: 3, color: '#999' }, // hybrid platform to academic

	        { from: 10, to: 33, color: '#999' }, // daz no non-academic partners
	        { from: 11, to: 33, color: '#999' }, // netzwerk architekturwissenschaft to non academic partners
	        { from: 13, to: 33, color: '#999' }, // wolkenkukuksheim to non academic partners
	        { from: 14, to: 33, color: '#999' }, // fathomable to non academic partners

	        { from: 999, to: 33, color: '#7b8fff', dashes: [5,5], width: 1 }, // tbd
	        { from: 999, to: 3333, color: '#7b8fff', dashes: [5,5], width: 1 }, // tbd
	        { from: 998, to: 33, color: '#7b8fff', dashes: [5,5], width: 1 }, // tbd
	        { from: 998, to: 3333, color: '#7b8fff', dashes: [5,5], width: 1 }, // tbd
	        { from: 997, to: 3, color: '#7b8fff', dashes: [5,5], width: 1 }, // tbd
	        { from: 997, to: 333, color: '#7b8fff', dashes: [5,5], width: 1 }, // tbd
	      ]),
	}

		/* ------------ */
		/* HEADER LINKS */
		/* ------------ */
		let headerLinks = document.getElementsByClassName("headerLink");
		let mobileNavBtn = document.getElementById("mobileNavBtn");
		let mobileLinks = document.querySelectorAll("nav.mobileNav .headerLink");
		function toggleMobileNav(){
			document.querySelector("nav.mobileNav").classList.toggle("active");
			mobileNavBtn.classList.toggle("active");
		}
		mobileNavBtn.addEventListener("click", () => {
			toggleMobileNav();
		});

		mobileLinks.forEach( (link) => link.addEventListener("click", () => toggleMobileNav() ));

		var teamNetwork = null;

	// when all DOM content has been loaded
	document.addEventListener("DOMContentLoaded", () => {

		// set up the about page text slides
		showSlides(slideIndex);


		/* ------------ */
		/* MAILTO LINKS */
		/* ------------ */
		let mailtoLinks = document.getElementsByClassName("mailtoLink");
		for(let i = 0; i < mailtoLinks.length; i++){
			let currentLink = mailtoLinks[i];
			currentLink.setAttribute("href", currentLink.getAttribute("href").replace("$$$-", "@"));
		}

		/* ------------ */
		/* LOCALE LINKS */
		/* ------------ */
		// set up the locale links 
		document.querySelectorAll(".localeENG").forEach( (item) => { item.classList.add("activeLocale"); } )
		let localeLinks = document.getElementsByClassName("localeLink");
		for(let i = 0; i < localeLinks.length; i++){
			localeLinks[i].addEventListener("click", () => {
				document.querySelectorAll("header .localeLink.active").forEach( (elm) => elm.classList.remove("active"));
				localeLinks[i].classList.add("active");
				document.querySelectorAll(".locale.activeLocale").forEach( (item) => { item.classList.remove("activeLocale"); } )
				document.querySelectorAll(".locale" + localeLinks[i].getAttribute("locale")).forEach( (item) => { item.classList.add("activeLocale"); } )
			});
		}

		// set up the footer logos to change image on hover
		initFooterLogos();
		// get the team/project phase time slider and listen on input
		let timeSlider = document.getElementById("timeSlider");
		timeSlider.value = 1;
		timeSlider.addEventListener("input", function(){
			refreshNodes(this.value);
			refreshPhaseDescription(this.value);
			// teamNetwork.fit();
		});

		// initialize the visjs network graph
		if(document.getElementById("teamNetwork")){
		  teamNetwork       = new vis.Network( document.getElementById("teamNetwork"), data, options);
		  refreshNodes(1);
		  refreshPhaseDescription(1);
		  // set the right zoom
		  teamNetwork.moveTo({ scale: .7, position: {x:0,y:0} });
		  // teamNetwork.fit();
		}

		// intialize the header links
		// headerLinks[0].classList.add("active"); // set the initially active header link
	});


		// let isReady = false;

		// Reveal.slide(0,4); // move to the animated logo slide which is temporarily situated at the end
		// Reveal.on('ready', e => {
		// 	initialize(e.currentSlide);
		// });
		// setTimeout( ()=>{ !isReady ? initialize( Reveal.getCurrentSlide() ) : void(0) },100); // fallback for stuck welcome screen on chrome

		// before slide change, check if is logo slide and toggle class
		// Reveal.on('slidechanged', e => {
		// 	Reveal.sync();
		// 	teamNetwork.moveTo({ position: { x:0,y:0 } });
		// 	// teamNetwork.fit();
		// 	document.querySelector("header a.headerLink.active")?.classList.remove("active");
		// 	document.getElementById("link" + e.currentSlide.id.toLowerCase()).classList.add("active");
		// 	// document.getElementById( "link" + e.currentSlide.id.toLowerCase() )?.classList.add("active");
		// });


} )();