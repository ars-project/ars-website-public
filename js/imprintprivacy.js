// imprintprivacy.js

// main.js

( () => {
	
	// set up the footer logos to change their color on mouseover
	var initFooterLogos = function(){
		let footerLogos = document.getElementsByClassName("footerLogo");
		// console.log(footerLogos)
		for( let i = 0; i < footerLogos.length; i++ ){
			let logoImage = footerLogos[i].getAttribute("src");
			footerLogos[i].addEventListener("mouseenter", (e) => {
				e.target.setAttribute("src", logoImage.split(".svg")[0] + "_purple.svg");
			});
			footerLogos[i].addEventListener("mouseleave", (e) => {
				e.target.setAttribute("src", logoImage);
			});
		}
	};


	// when all DOM content has been loaded
	document.addEventListener("DOMContentLoaded", () => {

		/* ------------ */
		/* LOCALE LINKS */
		/* ------------ */
		// set up the locale links 
		document.querySelectorAll(".localeENG").forEach( (item) => { item.classList.add("activeLocale"); } )
		let localeLinks = document.getElementsByClassName("localeLink");
		for(let i = 0; i < localeLinks.length; i++){
			localeLinks[i].addEventListener("click", () => {
				document.querySelector("header .localeLink.active").classList.remove("active");
				localeLinks[i].classList.add("active");
				document.querySelectorAll(".locale.activeLocale").forEach( (item) => { item.classList.remove("activeLocale"); } )
				document.querySelectorAll(".locale" + localeLinks[i].getAttribute("locale")).forEach( (item) => { item.classList.add("activeLocale"); } )
			});
		}

		// set up the footer logos to change image on hover
		initFooterLogos();


	});

} )();