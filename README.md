# ARS Projectwebsite

This is the source of the ARS project website. It is published here for documentation purposes. You may obtain a copy of the source for research purposes. You may not create any derivative works that build on the given source code or assets in this repository.

Any logos of institutions or photos of project members included in this repository are property of the respective institution or member. 

## External Ressources

This repository uses
- https://www.github.com/necolas/normalize.css (MIT License)
- https://visjs.github.io/vis-network/ (Apache 2.0 & MIT License)
- https://github.com/hakimel/reveal.js/ (MIT License)
